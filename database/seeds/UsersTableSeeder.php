<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Admin',
            'email' => 'admin22@gmail.com',
            'password'=>bcrypt('12345678'), 
            'phone'=> '0977773979',
            'address'=>'10 Nguyễn Huệ, Quận 1',
            'remember_token'=>md5(Carbon::now() . rand(1000,9999)),
            'role'=> 1,
        ]);
    }
}
