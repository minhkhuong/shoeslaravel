-  api ( GET )   :  url (/api/categories.json) "getCategories" Lấy thông tin tất cả danh mục , trả về 1 list danh mục  có dạng => [ {} , {} , {} ,.....];
-  api ( GET )   :  url (/api/categories/count.json) "countCategories" đếm danh mục
-  api ( POST)   :  url (/api/categories.json) "postCategories" Tạo 1 danh mục mới , trả về 1 object  {.....}
-  api ( PUT)    :  url (/api/categories/{id}.json) "putCategoryByID"  chỉnh sủa danh mục ( tên danh mục  ,... );
-  api ( DELETE ):  url (/api/categories/{id}.json) "delCategoryByID"  xóa 1 danh mục  trả về  1 object rỗng => {}



-  api ( GET )  :  url ( /api/products.json) "getProducts" Lấy tất cả thông tin sản phẩm có danh mục có phân trang ( keywork(SimplePagination) mỗi trang 10 sản phẩm )
                   trả về 1 list sản phẩm  có dạng => [ {} , {} , {} ,.....];
-  api ( GET )   :  url (/api/products/count.json) "countProduct" đếm sản phẩm
-  api ( GET )  :  url (/api/product/{id}.json) "getProductByID" Lấy thông tin detail sản phẩm có danh mục  , trả về json 1 sản phẩm 
-  api ( POST)  :  url (/api/products.json) "postProduct"  Tạo 1 san phẩm 
-  api ( PUT )  :  url (/api/product/{id}.json) "putProductByID" chỉnh sửa thông tin của sản phâm ( danh mục , giá , tên , số lượng ,...)
-  api ( DELETE ): url (/api/product/{id}.json) "delProductByID" xóa 1 sản phẩm kết quả trả về object rỗng => {}
