<?php

Route::get('/categories.json','Api\CategoryApiController@getCategories')->name('get_category');

Route::get('/categories/count.json','Api\CategoryApiController@countCategories')->name('count_category');

Route::post('/create-categories.json','Api\CategoryApiController@postCategories');

// Route::put('/categories/{id}.json','Api\CategoryApiController@putCategoryByID');
Route::put('/categories/{id}.json' ,'Api\CategoryApiController@updateCategories' );

Route::delete('/categories/{id}.json','Api\CategoryApiController@deleteCategories');

Route::get('/products.json','Api\ProductApiController@getProducts');

Route::get('/products/count.json','Api\ProductApiController@countProducts');

Route::post('/create-products.json','Api\ProductApiController@postProducts');

Route::put('/products/{id}.json','Api\ProductApiController@updateProducts');

Route::delete('/products/{id}.json','Api\ProductApiController@deleteProducts');
