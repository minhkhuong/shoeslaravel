<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// API



// Route::get('/', function () {
//     return view('welcome');
// });

//// GUEST
Route::get('/','Guest\HomeController@home')->name('home');
Route::get('/chi-tiet-san-pham/{id}','Guest\ProductController@proDetail')->name('detail');
Route::get('/chi-tiet-gio-hang','Guest\CartController@cartDetail')->name('cart');
Route::post('/btn-cart','Guest\CartController@btnCart')->name('btn_cart');
Route::get('/xoa-gio-hang','Guest\CartController@cartDelete')->name('cart_del');
Route::get('/thanh-toan','Guest\CartController@checkOut')->name('check_out');
Route::post('/thanh-toan-hoan-tat','Guest\CartController@postCheckOut')->name('post_check');
Route::get('/dang-nhap','Guest\LoginController@loginClient')->name('login_client');
Route::post('/khach-hang','Guest\LoginController@postLogin')->name('post_login_client');
Route::get('/dang-ky','Guest\LoginController@registerClient')->name('register_client');
Route::post('/khach-hang-dang-ky','Guest\LoginController@postRegister')->name('post_register_client');
Route::get('/dang-xuat','Guest\LoginController@getLogoutClient')->name('logout');
Route::post('/thay-doi-gio-hang/{id}','Guest\CartController@postUpdateCart')->name('update_cart');
Route::get('/thuong-hieu/{id}','Guest\HomeController@getCate')->name('get_cate');
//AJAX Gio hang
Route::post('/them-gio-hang','Guest\CartAjaxController@cartCreate')->name('create_cart');
Route::post('/tang-so-luong-sp-gio-hang/{id}','Guest\CartAjaxController@increaseCart')->name('cart_decrea');
Route::post('/giam-so-luong-sp-gio-hang/{id}','Guest\CartAjaxController@decreaseCart')->name('cart_increa');
Route::delete('/xoa-gio-hang','Guest\CartAjaxController@cartDelete')->name('cart_del');



/// ADMIN
Route::get('login','Admin\LoginController@getLogin')->name('ad_login');
Route::post('admin','Admin\LoginController@postLogin')->name('post_login');
Route::get('logout','Admin\LoginController@getLogout')->name('ad_logout');

Route::group(['prefix' => 'admin','middleware'=>['auth','Admin']],function(){
    Route::resource('categories','Admin\CategoryController')->names('ad_cate');
    Route::resource('products','Admin\ProductController')->names('ad_prod');
    Route::resource('contacts','Admin\ContactController')->names('ad_contact');
    Route::post('edit-contact/{id}','Admin\ContactController@editContact')->name('edit_cont');
    Route::resource('comments','Admin\CommentController')->names('ad_comment');
    Route::resource('orders','Admin\OrderController')->names('ad_order');
    Route::resource('slides','Admin\SlideController')->names('ad_slide');
});    
