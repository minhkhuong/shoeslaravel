@extends('layouts.admin.master');
@section('content')
<form action="{{route('ad_comment.update',['id'=>$comment->id])}}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT"> {!! csrf_field() !!}
@csrf
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
                <strong>Thông tin bình luận</strong>
            </div>
            <div class="card-body card-block">
                
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tên người đăng</label></div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="name" class="form-control" value="{{$comment->user->name)}}" disabled>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Sản phẩm</label></div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="name" class="form-control" value="{{$comment->product->name)}}" disabled>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Nội dung</label></div>
                    <div class="col-12 col-md-9">
                        <textarea name="content" id="textarea-input" rows="9"  class="form-control" value="" disabled>{{$comment->content}}</textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label class=" form-control-label">Trạng thái</label></div>
                    <div class="col col-md-9">
                        <div class="form-check">
                            <div class="radio">
                                <label for="radio1" class="form-check-label ">
                                    <input @if($comment->status == 1) checked @endif type="radio" id="radio1" name="status" value="1" class="form-check-input">Hiển thị trên web
                                </label>
                            </div>
                            <div class="radio">
                                <label for="radio2" class="form-check-label ">
                                    <input @if($comment->status == 0) checked @endif type="radio" id="radio2" name="status" value="0" class="form-check-input">Không hiển thị trên web
                                </label>
                            </div>                       
                    </div>
                    </div>
                </div>
                <div>
                    <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block" style="width:200px">
                        <span id="payment-button-amount">Lưu</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>    
@endsection