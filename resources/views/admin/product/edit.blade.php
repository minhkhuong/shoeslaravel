@extends('layouts.admin.master');
@section('content')
<form action="{{route('ad_prod.update',['id'=>$prod->id])}}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT"> {!! csrf_field() !!}
@csrf
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
                <strong>Sản Phẩm</strong>
            </div>
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Thương hiệu</label></div>
                    <div class="col-12 col-md-9">
                        <select class="form-control" name="category_id" id="" >
                            @foreach($cate as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('hot'))
                            <div class="alert alert-success" role="alert">
                                <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('hot') }}</strong></span>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tên sản phẩm</label></div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="name" class="form-control" value="{{old('name',$prod->name)}}">
                        @if ($errors->has('name'))
                            <p class="help is-danger"style="color:red">{{ $errors->first('name') }}</p>
                        @endif
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Thông tin</label></div>
                    <div class="col-12 col-md-9">
                        <textarea name="short_description" id="textarea-input" rows="9"  class="form-control" value="">{{old('short_description',$prod->short_description)}}</textarea>
                        @if ($errors->has('short_description'))
                            <p class="help is-danger"style="color:red">{{ $errors->first('short_description') }}</p>
                        @endif
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="file-input" class=" form-control-label">Hình ảnh</label></div>
                    <div class="col-12 col-md-9"><input type="file" id="file-input" name="image" style="height:200%" class="form-control-file"></div>
                    @if(!empty($prod->image) && $prod->image != '')
                        <img src="{{asset('image/product/'.$prod->image)}}" alt="" style="width:200px;200px; margin-left:316px">
                    @endif
                    @if($errors->has('image'))
                            <div class="alert alert-success" role="alert">
                                <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('image') }}</strong></span>
                            </div>
                            @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Số lượng</label></div>
                    <div class="col-12 col-md-9">
                        <input type="number" id="text-input" name="amount" class="form-control" value="{{old('amount',$prod->amount)}}">
                        @if ($errors->has('amount'))
                            <p class="help is-danger"style="color:red">{{ $errors->first('amount') }}</p>
                        @endif
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Giá</label></div>
                    <div class="col-12 col-md-9">
                        <input type="number" id="text-input" name="price" class="form-control" value="{{old('price',$prod->price)}}">
                        @if ($errors->has('price'))
                            <p class="help is-danger"style="color:red">{{ $errors->first('price') }}</p>
                        @endif
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label class=" form-control-label">Trạng thái</label></div>
                    <div class="col col-md-9">
                        <div class="form-check">
                            <div class="radio">
                                <label for="radio1" class="form-check-label ">
                                    <input @if($prod->status == 1) checked @endif type="radio" id="radio1" name="status" value="1" class="form-check-input">Hiển thị trên web
                                </label>
                            </div>
                            <div class="radio">
                                <label for="radio2" class="form-check-label ">
                                    <input @if($prod->status == 0) checked @endif type="radio" id="radio2" name="status" value="0" class="form-check-input">Không hiển thị trên web
                                </label>
                            </div>                       
                    </div>
                    </div>
                </div>
                <div>
                    <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block" style="width:200px">
                        <span id="payment-button-amount">Lưu</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>    
@endsection