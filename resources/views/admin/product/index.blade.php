@extends('layouts.admin.master')
@section('content') 
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <a href="{{route('ad_prod.create')}}"><button type="submit"class="btn btn-outline-primary">Thên sản phẩm</button></a>
        </div>
        <div class="card-body">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Thương hiệu</th>
                        <th>Tên sản phẩm</th>
                        <th>Thông tin</th>
                        <th>Hình ảnh</th>
                        <th>Số lượng</th>
                        <th>Giá</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
            <tbody>
            <?php $index=1 ?>
             @foreach($prod as $item)
                 <tr>
                    <td>{{$index++}}</td>
                    <td>{{$item->category->name}}</td>
                    <td>{{$item->name}}</td>
                    <td style="width:20%">{!! substr($item->short_description,0,90)!!}...</td>
                    <td><img src="{{asset('image/product/'.$item->image)}}" alt="" style="width:200px;100px"></td>
                    <td>{{$item->amount}}</td>
                    <td>{{number_format($item->price)}}</td>
                    <td>{{$item->status()}}</td>
                    <td>
                        <a href="{{route('ad_prod.edit',$item->id)}}"><i class="fa fa-edit" style="font-size:36px"></i></a>
                            <i class="fa fa-trash-o"style="font-size:36px;margin-left:10%" data-toggle="modal" data-target="{{ '#delete' . $item->id }}"></i>
                        <div class="ilv-btn-group">
                            <form action="{{route('ad_prod.destroy',$item->id)}}" method="post">

                                <input type="hidden" name="_method" value="DELETE">
                                {!! csrf_field() !!}
                                <div class="modal fade" id="{{ 'delete' . $item->id }}" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content  -->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Cảnh báo</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Dữ liệu sẽ bị xóa vĩnh viễn.Bạn có chắc là muốn như vậy?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-outline-danger active">Đồng ý</button>
                                                <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Không đồng ý</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </td>
                 </tr>
            @endforeach
            </tbody> 
            </table>
        </div>
    </div>
</div>
@endsection