@extends('layouts.admin.master')
@section('content')
 <div class="col-sm-12">
     <table class="table table-bordered" >
        <h4>Mã đơn hàng: {{$order->order_number}}</h4><br>
        <thead>
        <h4>Thông tin khách hàng</h4>
            <tr>
                <th scope="col">Tên khách hàng</th>
                <th scope="col">Địa chỉ</th>
                <th scope="col">Số điện thoại</th>
                <th scope="col">Email</th>
                <th scope="col">Ghi chú</th>
                <th scope="col">Hình thức thanh toán</th>
                               
            </tr>
            </thead>
                <tbody>
                    <tr>
                        <td>{{$order->customer_name}}</td>
                        <td>{{$order->customer_address}}</td>
                        <td>{{$order->customer_phone}}</td>
                        <td>{{$order->customer_email}}</td>
                        <td>{{$order->note}}</td>
                        <td>{{$order->type_checkout}}</td>
                                
                    </tr>
                </tbody>
                           
    </table>
</div>
<div class="col-sm-12">
    <table class="table table-bordered">
        <h4>Thông tin đơn hàng </h4>
        <thead>
            <tr>
                <th scope="col">Tên sản phẩm</th>
                <th scope="col">Giá</th>
                <th scope="col">Số lượng</th>
                <th scope="col">Tổng cộng</th>
                               
            </tr>
        </thead>
        <tbody>
             @foreach($order->products as $prod)
                <tr>
                               
                    <td>{{$prod->name}}</td>
                     <td>{{$prod->price}}</td>
                     <td>{{$prod->pivot->amount}}</td>
                    <td class="total-col">{{number_format(($prod->price)*($prod->pivot->amount))}}</td>
                                   
                </tr>
                                
            @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td>Tổng tiền</td>
                    <td>{{number_format($order->total)}} VNĐ</td>
                </tr>
        </tbody> 
                           
    </table>
    <form action="{{route('ad_order.update',['id'=>$order->id])}}" method="post"> 
    <input type="hidden" name="_method" value="PUT"> {!! csrf_field() !!}
    @csrf
        <div class="form-group" style="width:50%">
            <label for="exampleFormControlSelect1">Trạng thái giao hàng</label>
            <select class="form-control" id="exampleFormControlSelect1" name="trangthai"
             @if($order->order_status=='GR') disabled @endif @if($order->order_status=='H') disabled @endif >
                <option value='M' @if($order->order_status=='M') selected @endif>Đơn hàng mới</option>
                <option  @if($order->order_status=='DG') selected @endif value='DG'>Đơn hàng đang giao</option>
                <option  @if($order->order_status=='GR') selected @endif value='GR'>Đơn hàng đã giao</option>
                <option  @if($order->order_status=='H') selected @endif value='H'>Đơn hàng đã hủy</option>
                        
            </select>
        </div>
        <div>
        @if(!($order->order_status=='GR') && !($order->order_status=="H")) 
        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block"style="width:40%">
            <span id="payment-button-amount">Lưu</span>
        </button>
        @endif
        
    </div>
    </form>
    
</div>

@endsection