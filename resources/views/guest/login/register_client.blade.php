@extends('layouts.guest.master')
@section('content')
<section class="login_box_area p_120">
		<div class="container">
				<div class="col-lg-12">
					<div class="login_form_inner reg_form">
						<h3>Tạo tài khoản</h3>
						<!-- @if ($errors->any())
    						<div class="alert alert-danger">
        						<ul>
            					@foreach ($errors->all() as $error)
                					<li>{{ $error }}</li>
           						 @endforeach
        						</ul>
   							 </div>
   						 @endif   -->
						<form class="row login_form" action="{{route('post_register_client')}}" method="post" id="contactForm" novalidate="novalidate">
							@csrf
							<div class="col-md-12 form-group">
								<input type="text" class="form-control" id="name" name="name" placeholder="Họ và tên">
							</div>
							<div class="col-md-12 form-group">
								<input type="text" class="form-control" id="name" name="address" placeholder="Địa chỉ">
							</div>
							<div class="col-md-12 form-group">
								<input type="text" class="form-control" id="name" name="phone" placeholder="Số điện thoại">
							</div>
							<div class="col-md-12 form-group">
								<input type="email" class="form-control" id="email" name="email" placeholder="Email">
							</div>
							@if ($errors->has('email'))
                                <p class="help is-danger" style="color:red; font-family:Verdana" >{{ $errors->first('email') }}</p>
                            @endif
							<div class="col-md-12 form-group">
								<input type="password" class="form-control" id="password" name="password" placeholder="Mật khẩu">
							</div>
							@if ($errors->has('password'))
                                <p class="help is-danger" style="color:red; font-family:Verdana" >{{ $errors->first('password') }}</p>
                            @endif
							<div class="col-md-12 form-group">
								<input type="password" class="form-control" id="password" name="password_confirmation" placeholder="Nhập lại mật khẩu">
							</div>
							@if ($errors->has('password_confirmation'))
                                <p class="help is-danger" style="color:red; font-family:Verdana" >{{ $errors->first('password_confirmation') }}</p>
                            @endif
							<div class="col-md-12 form-group">
								
							</div>
							<div class="col-md-12 form-group">
								<button type="submit" class="btn submit_btn">Đăng ký</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>s
@endsection