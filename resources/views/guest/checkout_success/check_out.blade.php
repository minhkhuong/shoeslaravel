@extends('layouts.guest.master')
@section('content')
<section class="checkout_area section_gap">
	<div class="container">
		<form  action="{{route('post_check')}}" method="post">
			@csrf()
			<div class="billing_details">
				<div class="row">
					<div class="col-lg-8">
						<h3>Thông tin hóa đơn</h3>
							<div class="col-md-12 form-group">
								<input type="text" class="form-control" name="customer_name" placeholder="Họ và tên">
								@if ($errors->has('customer_name'))
                                    <p class="help is-danger" style="color:red; font-family:Verdana" >{{ $errors->first('customer_name') }}</p>
                            	@endif
							</div>
							<div class="col-md-12 form-group">
								<input type="text" class="form-control"  name="customer_address" placeholder="Địa chỉ">
								@if ($errors->has('customer_address'))
                                    <p class="help is-danger" style="color:red; font-family:Verdana" >{{ $errors->first('customer_address') }}</p>
                            	@endif
							</div>
							<div class="col-md-12 form-group">
								<input type="text" class="form-control"  name="customer_phone" placeholder="Số điện thoại">
								@if ($errors->has('customer_phone'))
                                    <p class="help is-danger" style="color:red; font-family:Verdana" >{{ $errors->first('customer_phone') }}</p>
                            	@endif
                            </div>
                            <div class="col-md-12 form-group">
								<input type="text" class="form-control"  name="customer_email" placeholder="Địa chỉ email">
								@if ($errors->has('customer_email'))
                                    <p class="help is-danger" style="color:red; font-family:Verdana" >{{ $errors->first('customer_email') }}</p>
                            	@endif
							</div>
							<div class="col-md-12 form-group">
								<div class="creat_account">
								</div>
								<input type="text" class="form-control"  name="note" placeholder="Ghi chú">
							</div>
					</div>
					<div class="col-lg-4">
                        <div class="order_box" style="width:500px"> 
							<h2>Đơn hàng của bạn</h2>
							<ul class="list">
								<li>
									<a href="#">Sản phẩm
										<span style="margin-right:30px">Giá</span>
									</a>
                                </li>
                                
                                @foreach($checkout as $item)
								<li>
									<a href="#">{{$item->name}}
										<span class="middle">{{$item->qty}}</span>
										<span class="last">{{number_format($item->price)}}đ</span>
									</a>
                                </li>
                                @endforeach
							</ul>
							<ul class="list list_2">
								<li>
									<a href="#">Tổng cộng
										<span>{{number_format(Cart::subtotal(0,'.', '')) }}đ</span>
									</a>
								</li>
								
							</ul>
							<div class="payment_item">
								<input type="submit" class="main_btn" style="width:440px" name="cod" value="Thanh toán">
								<!-- <a class="main_btn" href="">Thanh toán</a> -->
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</form>			
	</div>
</section>
@endsection