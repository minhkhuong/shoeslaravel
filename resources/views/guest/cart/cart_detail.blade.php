@extends('layouts.guest.master')
@section('content')
@include('share.modal')	
<section class="cart_area"style="margin-top:70px">
		<div class="container">
			<div class="cart_inner">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Product</th>
								<th scope="col">Price</th>
								<th scope="col">Quantity</th>
								<th scope="col">Total</th>
								<th scope="col">UpdateCart</th>
								<th scope="col"></th>

							</tr>
						</thead>
						@if(Session::has('errorcheckProductQTY'))
					<div class="alert alert-info" role="alert">
						<span class="invalid-feedback" style="display:block;">
						<strong>{{ Session::get('errorcheckProductQTY') }}</strong></span>
					</div>
				@endif()
				@if(Session::has('errorProductItem'))
					<div class="alert alert-info" role="alert">
						<span class="invalid-feedback" style="display:block;">
						<strong>{{ Session::get('errorProductItem') }}</strong></span>
					</div>
				@endif()
				@if(Session::has('errorItemCart'))
					<div class="alert alert-info" role="alert">
						<span class="invalid-feedback" style="display:block;">
						<strong>{{ Session::get('errorItemCart') }}</strong></span>
					</div>
				@endif()
						<tbody>					
                            @foreach($carts as $item)
							<form action="{{route('update_cart',['id'=>$item->rowId])}}" method="post">
							@csrf
							<tr>
								<td>
									<div class="media">
										<div class="d-flex">
											<img src="{{asset('image/product/'.$item->options->image)}}"  style="width:150px"alt="">
										</div>
										<div class="media-body">
											<p>{{$item->name}}</p>
										</div>
									</div>
								</td>
								<td>
									<h5>{{number_format($item->price)}}</h5>
								</td>
								<td>
									<div class="product_count">
										<input type="text" name="amount" id="quantityProduct-{{$item->rowId}}" data-id="{{$item->rowId}}"   value="{{$item->qty}}" min="1" title="Quantity:" class="input-text qty">
										<button id="btnTangCart-{{$item->rowId}}"
										 class="increase items-count" type="button">
											<i class="lnr lnr-chevron-up"></i>
										</button>
										<button id="btnGiamCart-{{$item->rowId}}"
										 class="reduced items-count" type="button">
											<i class="lnr lnr-chevron-down"></i>
										</button>
										<script>	
											$(document).ready(function(){
												$('#btnTangCart-{{$item->rowId}}').on('click',function(){
													var tang = tangGiam(parseInt(1));
												});
												$('#btnGiamCart-{{$item->rowId}}').on('click',function(){
													var tang = tangGiam(parseInt(-1));
												});
												function tangGiam(number){
													const qty = $('#quantityProduct-{{$item->rowId}}').val();
													// lay so luong max
													const qtyMax = $('#quantityProduct-{{$item->rowId}}').attr('max');
													//lay so luong min
													const qtyMin = $('#quantityProduct-{{$item->rowId}}').attr('min');
													
													const newQty = parseInt(qty)+parseInt(number);
													$('#quantityProduct-{{$item->rowId}}').val(newQty);
													if(newQty >= qtyMax){
														$('#quantityProduct-{{$item->rowId}}').val(qtyMax);
													}
													if(newQty <= qtyMin){
														$('#quantityProduct-{{$item->rowId}}').val(qtyMin);
													}
												}
											});
										</script>
									</div>
								</td>
								<td>
									<h5>{{number_format(($item->qty)*($item->price))}}đ</h5>
								</td>
								<td>
									<input type="submit" class="gray_btn" value="Update">
								</td>
								<td>
									<input type="submit" class="gray_btn" value="Xóa">
								</td>
                            </tr>
							</form>
                            @endforeach
						
							
							<tr>
							<td>
									<a class="gray_btn" href="{{route('cart_del')}}">Xóa tất cả</a>
								</td>
								<td></td>
								<td></td>
								<td>
									<h5>Tổng cộng</h5>
								</td>
								<td>
									<h5>{{number_format(Cart::subtotal(0,'.', '')) }}đ</h5>
								</td>
							</tr>
							
							<tr class="out_button_area">
								<td>

								</td>
								<td>

								</td>
								<td>

								</td>
								<td>

								</td>
								<td>

								</td>
								<td>
									<div class="checkout_btn_inner">
										<a class="gray_btn" href="{{route('home')}}">Continue Shopping</a>
										<a class="main_btn" href="{{route('check_out')}}">Proceed to checkout</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	
@endsection