@extends('layouts.guest.master')
@section('content')
<section class="cat_product_area section_gap">
		<div class="container-fluid">
			<div class="row flex-row-reverse">
				<div class="col-lg-9">
					<div class="product_top_bar">
						<div class="left_dorp">
							<h1>{{$nameCate->name}}</h1>
						</div>
						<div class="right_page ml-auto">
							<nav class="cat_page" aria-label="Page navigation example">
								
							</nav>
						</div>
					</div>
					<div class="latest_product_inner row">
			@if(count($cate)>0)
            @foreach($cate as $item)
            @if($item->category->status == 1)
			<div class="col-lg-3 col-md-3 col-sm-6">
            	<form action="" method="post">
              		@csrf
                		<input type="hidden" name="id" value="{{$item->id}}">
						<input type="hidden" name="name" value="{{$item->name}}">
						<input type="hidden" name="image" value="{{$item->image}}">
						<input type="hidden" name="price" value="{{$item->price}}">
						<input type="hidden" name="amount" value="1">
					<div class="f_p_item">    
						<a href="{{route('detail',$item->id)}}">
						<div class="f_p_img">
                       		<a href="{{route('detail',$item->id)}}">
                            	<img class="img-fluid" id="image" src="{{asset('image/product/'.$item->image)}}" style="width:300px; height:200px" alt="">
							</a>
						</div>
						</a>
						<h5>{{$item->name}}</h5>
						<h5>{{number_format($item->price)}}</h5>
			  		</div>
			  
				</form>  
			</div>
            @endif
            @endforeach
			@else
			<div>
			<p>Không có sản phẩm nào tìm thấy</p></div>
			@endif
					</div>
				</div>
				<div class="col-lg-3">
					<div class="left_sidebar_area">
					<!-- <form action="">
						<aside class="left_widgets p_filter_widgets">
							<div class="l_w_title">
								<h3>Product Filters</h3>
							</div>
							<div class="widgets_inner">
							<div class="widgets_inner">
								<h4>Tên sản phẩm</h4>
								<div class="range_item">
									<div id="slider-range"></div>
									<div class="row m0">
										<input class="form-control" type="text" id="amount" name="searchName" placeholder="Search">
									</div>
								</div>
							</div>
								<h4>Thương hiệu</h4>
								@foreach($cate as $item)
								<ul class="list">
									<input type="radio" name="searchCategory" id="cate" value="{{$item->id}}" />&nbsp<label for="crust1">{{$item->name}}</label>
								</ul>
								@endforeach
							</div>
							<div class="widgets_inner">
								<h4>Gía</h4>
								<ul class="list">
									<li>
										<input name="searchPrice" type="radio" value="1">1.000.000-2.000.000
									</li>
									<li>
										<input name="searchPrice" type="radio" value="2">2.000.000-4.000.000
									</li>
									<li >
										<input  name="searchPrice" type="radio" value="3">4.000.000-10.000.000
									</li>
									<li>
										<input name="searchPrice" type="radio" value="4">10.000.000-20.000.0000
									</li>
										
								</ul>
							</div>
							
							<input type="submit" class="form-control"value="Tìm kiếm">
						</aside>
						
					</form> -->
					</div>
				</div>
			</div>

		</div>
	</section>
   
@endsection