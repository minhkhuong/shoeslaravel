<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Http\Requests\CreateProduct;
use App\Http\Requests\UpdateProduct;


class ProductApiController extends Controller
{
    public function getProducts()
    {
        $products = Product::with('category')->where('status','1')->get();
        // dd($products);
        $arrayproduct = [];
        foreach($products as $product){
            // dd($product->category);
            $infoProducts =[
                'category'=>$product['category']['name'],
                'name' =>  $product['name'],
                'slug' => $product['slug'],
                'short_description' =>$product['short_description'],
                'price'=>$product['price'],
                'amount'=>$product['amount'],
               
            ]; 
            array_push($arrayproduct,$infoProducts);
        }
        return response()->json([
            
            'product'=>$arrayproduct,
            // 'category'=>$arrayproduct,

        ],200);
    }

    public function countProducts()
    {
        $countproducts = Product::where('status','1')->count();

        return response()->json([
            'count'=>$countproduct,
        ],200);
    }

    public function postProducts(CreateProduct $request)
    {
        $input_data = $request->validated();

        $input_data = $input_data['product'];

        $name= $input_data['name'];

        $status = $input_data['status'];

        $short_description =$input_data['short_description'];

        $amount = $input_data['amount'];

        $price = $input_data['price'];

        $image= $input_data['image'];

        $slug = str_slug($name,'-');

        $category_id = $input_data['category_id'];
        
        // dd($category_id);

        $product = new Product();

        $product->name = $name;
        $product->status = $status;
        $product->short_description = $short_description;
        $product->amount = $amount;
        $product->price = $price;
        $product->slug = $slug;
        $product->image = $image;
        $product->category_id= $category_id;
        $product->save();

        $products = Product::with('category')->where('id',$product->id)->first();
        // dd($produtsss);
        // dd($product);
        // dd($product['category']);

        $infoProd=[
            'category'=> $products['category']['name'],
            'name'=>$products['name'],
            'short_description'=>$products['short_description'],
            'slug'=>$products['slug'],
            'price'=>$products['price'],
            'amount'=>$products['amount'],
        ];
        return response()->json([
            // 'product'=>$product,
            'product'=>$infoProd,

        ],201);
    }

    public function updateProducts(UpdateProduct $request, $id)
    {
        $product = Product::findOrFail($id);

        $input_data = $request->validated();

        $input_data = $input_data['product'];

        $name = $input_data['name'];

        $short_description = $input_data['short_description'];

        $amount = $input_data['amount'];

        $category_id = $input_data['category_id'];

        $product->name = $name;
        $product->short_description = $short_description;
        $product->amount = $amount;
        $product->category_id = $category_id;
        $product->save();

        return response()->json([
            'product'=>$product,
        ],200);
    }

    public function deleteProducts($id)
    {
        $product = Product::findOrFail($id);

        $product->delete();

        return response()->json(new \stdClass(), 200);
    }
}
