<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\CreateCategory;
use App\Http\Requests\UpdateCategory;

class CategoryApiController extends Controller
{
    public function getCategories()
    {
        $cate = Category::where('status','=','1')->get();

        return response()->json(
            [
                'categories'=>$cate,
            ],
        200);
    }
    public function countCategories()
    {
        $couCate = Category::where('status','1')->count();

        return response()->json([

            'count'=>$couCate,

        ],200);
    }


    public function postCategories(CreateCategory $request)
    {
        $input_data = $request->validated();
       
        $input_data = $input_data['category'];

        $name = $input_data['name'];

        $description = $input_data['description'];

        $status = $input_data['status'];

        $image = $input_data['image'];

        $slug = str_slug($name, '-');

        $category = new Category();

        $category->name = $name;
        $category->description = $description;
        $category->status = $status;
        $category->image = $image;
        $category->slug= $slug;

        $category->save();

        return response()->json(
            [
                'category'=>$category,
            ],201);
    }

    public function updateCategories(UpdateCategory $request , $id)
    {

        $category = Category::findOrFail($id);

        $input_data = $request->validated();
        // dd($input_data);    
        $input_data = $input_data['category'];
        // dd($input_data);
        $name = $input_data['name'];

        $description = $input_data['description'];
        // dd($requ);
    

        $category->name= $name;

        $category->description = $description;

        $category->save();

        return response()->json
        ([
            'category'=>$category,
        ],200);

    }

    public function deleteCategories($id)
    {
        $category = Category::findOrFail($id);

        $category->delete();

        return response()->json(new \stdClass(), 200);
    }
}
