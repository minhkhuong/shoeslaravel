<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Silde;
use Auth;
use App\User;


class HomeController extends Controller
{
    public function home(Request $request){
        $searchCate = $request->input('searchCategory');
        $searchPrice = $request->input('searchPrice');
        $searchName = $request->input('searchName');
        $pro = Product::with('category')->whereStatus(1)->filterCategory($searchCate)->filterPrice($searchPrice)->filterName($searchName)->paginate(4);
        $cate = Category::whereStatus(1)->get();
        return view('guest.home.home')->with([
            'pro'=>$pro,
            'cate'=>$cate,
        ]);
    }
    public function getCate($id){
        $cate = Product::with('category')->whereStatus(1)->where('category_id',$id)->get();
        $nameCate = Category::where('id',$id)->first();
        return view('guest.product.probycategory')->with([
            'cate'=>$cate,
            'nameCate'=>$nameCate,
        ]);
    }
    
}
