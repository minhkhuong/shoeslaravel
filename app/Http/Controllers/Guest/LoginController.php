<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Hash;
use Cart;
use Auth;

class LoginController extends Controller
{
    public function loginClient()
    {
        return view('guest.login.login_client');
    }
    public function postLogin(Request $req)
    {
        $rules=[
            'email'=>'required|email',
            'password'=>'required|min:8',
        ];
        $mess=[
            'email.required'=>'Vui lòng không để trống email',
            'email.email'=>'Không đúng định dạng email',
            'password.required'=>'Vui lòng nhập mật khẩu',
            'password.min'=>'Mật khẩu ít nhất có 8 ký tự',
        ];
        $validator = Validator::make($req->all(),$rules, $mess);
        if($validator->fails()){
            return redirect()->back()->withError($validator)->withInput();
        }else{
            $email = $req->input('email');
            $pass = $req->input('password');
            if(Auth::attempt(['email'=>$email,'password'=>$pass])){
                return redirect()->route('home');
            }else{
                return redirect()->back()->with('errorLogin',"Email hoặc mật khẩu sai vui lòng nhập lại");
            }
            return redirect()->back()->with('errorLogin','Anh/Chị không thể truy cập trang này');
        }
    }
    public function registerClient()
    {
        return view('guest.login.register_client');
    }
    public function postRegister(Request $req)
    {
   
        $this->validate(
            $req,
            [
                'email'=>'required|unique:users|email',
                'password'=>'required|min:8',
                'password_confirmation'=>'required|same:password',
            ],
            [
                'email.required'=>'Vui lòng điển email',
                'email.unique'=>'Email này đã tồn tại',
                'email.email'=>'Email không đúng định dạng',
                'password.required'=>'Vui lòng nhập mật khẩu',
                'password.min'=>'Mật khẩu chứa ít nhất 8 ký tự',
                'password_confirmation.required'=>'Vui lòng nhập lại mật khẩu',
                'password_confirmation.same'=>'Mật khẩu không chính xác',
            ]
            );
        $user = new User();
        $user->email = $req->email;
        $user->name = $req->name;
        $user->address = $req->address;
        $user->phone = $req->phone;
        $user->password = Hash::make($req->password);
        $user->role = 0;
        $user->save();
   
        return redirect()->back()->with('success','Đăng ký thành công');
    }
    public function getLogoutClient()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
