<?php

namespace App\Http\Controllers\Guest;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function proDetail($id){
        $detail = Product::where('id',$id)->first();
        return view('guest.product.product_detail')->with([
            'detail'=>$detail,
        ]);
    }
}
