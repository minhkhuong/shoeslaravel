<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Models\Order;
use App\Models\Product;
use Auth;
use Validator;

class CartController extends Controller
{
    public function cartDetail()
    {
        $cartItem = Cart::content();
        $product = Product::all();
        return view('guest.cart.cart_detail')->with(['carts'=>$cartItem , 'products' => $product]);
    }
    public function btnCart(Request $req)
    {
        Cart::add([
            'id' => $req->id,
            'name' => $req->name,
            'qty' => $req->amount,
            'price' => $req->price,
            'options' => ['image' => $req->image],
        ]);
        // lay san pham dang  chuan bi them vao gio hang
        $product = Product::whereId($req->id)->first();
        // lay so luong ton` kho cua san pham do 
        $quantity = $product->amount ;
        // ktra so dat   voi so luong trong kho
        if($req->amount > $quantity){
            return back()->with(['danger' => 'Số lượng vượt quá quy định cho phép']);
        }
        return redirect()->route('cart');
    }
    public function cartDelete()
    {
        Cart::destroy();
        return redirect()->back();
    }
    public function checkOut()
    {
        $checkout = Cart::content();
        return view('guest.checkout_success.check_out')->with(['checkout'=>$checkout]);
    }      
    public function postCheckOut(Request $req)
    {
        $check=[
            'customer_name'=>'required',
            'customer_address'=>'required|max:50',
            'customer_phone'=>'required|max:10',
            'customer_email'=>'required|email',
        ];
        $mess=[
            'customer_name.required'=>'Vui lòng nhập họ tên của anh/chị',
            'customer_address.required'=>'Vui lòng nhập địa chỉ của anh/chị',
            'customer_phone.required'=>'Vui lòng nhập số điện thoại của anh/chị',
            'customer_phone.max'=>'Vui lòng nhập đủ 10 số',
            'customer_email.required'=>'Vui lòng nhập email của bạn',
            'customer_email.email'=>'Vui lòng nhập đúng định dạng email',
        ];

        $validator = Validator::make($req->all(), $check, $mess);
        if($validator->fails()){
            return redirect()->route('check_out')->withErrors($validator)->withInput();
        }else{
            $order = new Order;
            $order->order_number = 'HD'.rand(0,10000);
            $order->total = str_replace(',','', Cart::subtotal());
            $order->customer_name = $req->customer_name;
            $order->customer_address = $req->customer_address;
            $order->customer_phone = $req->customer_phone;
            $order->customer_email = $req->customer_email;
            $order->note = $req->note;
            $order->order_status = 'M';
            if($req->cod){
                $order->type_checkout="COD";
                $order->save();
            }
            $order->save();
            $cartInfo = Cart::content();
            if(count($cartInfo)>0){
                $arr_id = [];
                foreach($cartInfo as $key => $item){
                    $masp = $item->id;
                    $sln = $item->qty;
                    $sanpham = Product::find($masp);
                    $sanpham->amount -=$sln;
                    $sanpham->save();
                    $array[$key]=[
                        'order_id' => $order->id,
                        'product_id' => $item->id,
                        'amount' => $item->qty,
                    ];
                    $order->products()->sync($array);
                }
            }
            if($req->cod){
                Cart::destroy();
                return redirect()->route('home');
            }
        }
    }
    public function postUpdateCart(Request $request, $id)
    {
       $getItem = Cart::get($id);  // Lay thong tin san pham trong gio hang co do id san pham   
       if(empty($getItem)){
           return redirect()->back()->with('errorItemCart','Không tìm thấy sản phẩm thay đổi');
       } 
       $productID = $getItem->id; // lay id tu getitem
       $productItem = Product::where('id',$productID)->first(); // tim  san pham co id 
       if(empty($productItem)){
           return redirect()->back()->with('errorProductItem','Không tìm thấy từng sản phẩm');
       }
       $productQTY =  $productItem->amount; // Lay so luong trong kho
       $updateQTY = $request->input('amount'); // so luong dc gui len
       if($updateQTY > $productQTY){
        return redirect()->back()->with('errorcheckProductQTY','Số lượng vượt quá quy định');
       }
      
       Cart::update($id, $updateQTY);
       return redirect()->back();
    }
    
}
