<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Models\Order;
use App\Models\Product;
use Auth;
use Validator;

class CartAjaxController extends Controller
{
    public function cartCreate(Request $request)
    {
        
        $id = $request->id;

        $product = Product::whereId($id)->first();

        if(empty($product)){
            return "Sản phẩm không tồn tại";
        }

        $name = $product['name'];

        $price = $product['price'];

        $image = $product['image'];

        $cart = Cart::add([
            'id'=>$id,
            'name'=>$name,
            'qty'=>$request->amount,
            'price'=>$price,
            'options'=>['image'=>$image],
        ]); 
        $cartItem = Cart::content();
    
        return response()->json($cartItem,201);
    }

    public function increaseCart(Request $request, $id)
    {

        // dd(Cart::content());
        $getItemIncrea = Cart::get($id);
        // dd($getItem);       
        if(empty($getItemIncrea)){
            $json = [
                'fail'=>'Không tìm sản phẩm có trong giỏ hàng',
            ];
            return response()->json($json,404);
        }
        $productIdIncrea = $getItemIncrea->id;
        $productItemIncrea = Product::where('id',$productIdIncrea)->first();
        if(empty($productItemIncrea)){
            $json=[
                'fail'=>'Không tìm thấy sản phẩm trong kho',
            ];
            return response()->json($json,404);
        }
        $updateQtyIncrea = ($getItemIncrea->qty);
        $increaQty = $updateQtyIncrea+1;
        $productQtyIncrea = $productItemIncrea->amount;//So luong trong kho
        
        if($updateQtyIncrea >= $productQtyIncrea){
            $json =[
                'fail'=>'Số lượng vượt quá quy định',
            ];
            return response()->json($json,404);
        } 
        // dd($increaQty);
        $updateCartIncrea = Cart::update($id,$increaQty);
        // dd($updateCart);
        $getCartIncrea = Cart::get($id);
        $upCartQtyIncrea = $getCartIncrea->qty;
        // dd($upCartQty);
        $upCartPriceIncrea = $getCartIncrea->price;
        // dd($upCartPrice);   
        $totalIncrea = ($upCartQtyIncrea * $upCartPriceIncrea);
        // dd($total);
        $subTotalIncrea = Cart::subtotal(0,'.', '');
        $json=[
            'getCartIncrea'=>$getCartIncrea,
            'totaIncreal'=>$totalIncrea,
            'subtotalIncrea'=>$subTotalIncrea,
        ];
        return response()->json($json,200);
        // dd($subtotal);
    }

    public function decreaseCart(Request $request  ,$id)
    {
        // Cart::destroy();
        // dd(Cart::content(),Cart::subtotal(0,'.', ''));
        $getItem = Cart::get($id);
        // dd($getItemDecrea);
        if(empty($getItem)){
            $json=[
                'fail'=>'Sản phẩm này không tồn tại giỏ hàng',
            ];
            return response()->json($json,404);
        }
        $productIdDecrea = $getItem->id;
        // dd($productIdDecrea);
        $productItemDecrea = Product::where('id',$productIdDecrea)->first();
        // dd($productItemDecrea);
        if(empty($productItemDecrea)){
            $json=[
                'fail'=>'Sản phẩm này không tồn tại trong kho',
            ];
            return response()->json($json,404);
        }
        $qty = $getItem->qty;
        // dd($updateQtyDecrea);   
        if($qty == 1){
            // dd('asdsad');
            Cart::remove($id);
            $json=[
                'subtotal'=> Cart::subtotal(0,'.', ''),
            ];
            return response()->json($json,200);
        }
        $decreaQty = (int)($qty - 1);
        // dd($decreaQty);
        dd(Cart::update($id, (int)$decreaQty));


        $getCartDecrea = Cart::get($id);
        // dd($getCartDecrea); 
        $upCartQtyDecrea = $getCartDecrea->qty;

        $upCartPriceDecrea = $getCartDecrea->price;

        $total =  $upCartQtyDecrea * $upCartPriceDecrea ;
        // dd($total);
        
    }
}
