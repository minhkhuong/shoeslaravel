<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cate = Category::orderBy('id','desc')->paginate(10);
        return view('admin.category.index')->with(['cate'=>$cate]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'=>'required',
                'description'=>'required',
            ],
            [
                'name.required'=>'Không được để trống tên thương hiệu',
                'description.required'=>'Không được để trống thông tin',
            ]
            );
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $cate= new Category();
            $cate->name = $request->name;
            $cate->slug = str_slug($request->name).rand(100000, 999999);
            $cate->description = $request->description;
            $cate->parent_id = 0;
            $cate->status = $request->status;
            if($request->hasFile('image')){
                $file = $request->image;
                $new_image_name = time().$file->getClientOriginalName();
                $cate->image = $new_image_name;
                $file->move('image/category',$new_image_name);
            }
            $cate->save();
            return redirect()->route('ad_cate.index')->with(['mess'=>'Đã lưu']);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cate = Category::find($id);
        return view('admin.category.edit',compact('cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cate = Category::findOrFail($id);
        $validator = 
        Validator::make(
            $request->all(),
            [
                'name'=>'required',
                'description'=>'required',
            ],
            [
                'name.required'=>'Không được để trống tên thương hiệu',
                'description.required'=>'Không để trống thông tin',
            ]
            );
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $cate->name = $request->name;
            $cate->slug = str_slug($request->name).rand(100000, 999999);
            $cate->description = $request->description;
            $cate->status = $request->status;
            $cate->parent_id = 0;
            if($request->hasFile('image')){
                $file = $request->image;
                $new_image_name = time().$file->getClientOriginalName();
                $cate->image = $new_image_name;
                $file->move('image/category',$new_image_name);
            }
            $cate->save();
            return redirect()->route('ad_cate.index')->with(['mess'=>'Cập nhật thành công']);
        }    

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cate = Category::findOrFail($id);
        $cate->delete();
        return redirect()->back();
    }
}
