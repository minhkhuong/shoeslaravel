<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Hash;
use Auth;
use App\User;
use Validator;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('admin.login.login');
    }
    public function postLogin(Request $req)
    {
        $rules=[
            'email'=>'required',
            'password'=>'required|min:8',
        ];
        $message=[
            'email.required'=>'Không được để trống email',
            'email.email'=>'Email không đúng định dạng',
            'password.required'=>'Không được để trống mật khẩu',
            'password.min'=>'Mật khẩu ít nhất phải có 8 ký tự',
        ];
        $validator = Validator::make($req->all(),$rules,$message);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $email = $req->input('email');
            $password = $req->input('password');
            if(Auth::attempt(['email'=>$email,'password'=>$password])){
                if(Auth::user()->role == 1){
                    return redirect()->route('ad_cate.index');
                }
            }else{
                return redirect()->back()->with('errorLogin',"Email hoặc mật khẩu sai vui lòng nhập lại");
            }
            return redirect()->back()->with('errorLogin',"Bạn không thể truy cập trang này");
        }
    } 
    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('ad_login');
    }
}
