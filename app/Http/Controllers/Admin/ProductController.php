<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Validator;
use App\Models\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prod = Product::orderBy('id','desc')->paginate(5);
        $cate = Category::whereStatus(1)->get();
        return view('admin.product.index')->with([
            'prod'=>$prod,
            'cate'=>$cate,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cate = Category::all();
        return view('admin.product.create')->with([
            'cate'=>$cate,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'=>'required',
                'short_description'=>'required',
                'amount'=>'required|numeric',
                'price'=>'required|numeric|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            ],
            [
                'name.required'=>'Không được để trống tên sản phẩm',
                'short_description.required'=>'Không được để trống thông tin sản phẩm',
                'amount.required'=>'Không được để trống số lượng sản phẩm',
                'price.required'=>'Không để được để trống giá sản phẩm '
            ] 
            ); 
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $prod = new Product();
            $prod->name = $request->name;
            $prod->slug = str_slug($request->name).rand(100000, 999999);
            $prod->short_description = $request->short_description;
            $prod->status = $request->status;
            if($request->hasFile('image')){
                $file = $request->image;
                $new_image_name = time().$file->getClientOriginalName();
                $prod->image=$new_image_name;
                $file->move('image/product',$new_image_name);
            }
            $prod->amount = $request->amount;
            $prod->price = $request->price;
            $prod->category_id = $request->category_id;
            $prod->save();
            return redirect()->route('ad_prod.index')->with(['mess'=>'Đã lưu']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cate = Category::all();
        $prod = Product::find($id);
        return view('admin.product.edit')->with([
            'prod'=>$prod,
            'cate'=>$cate,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cate = Category::all();
        $prod = Product::findOrFail($id);
        $validator = Validator::make(
            $request->all(),
            [
                'name'=>'required',
                'short_description'=>'required',
                'amount'=>'required|numeric',
                'price'=>'required|numeric',
            ],
            [
                'name.required'=>'Không được để trống tên sản phẩm',
                'short_description.required'=>'Không được để trống thông tin sản phẩm',
                'amount.required'=>'Không được để trống số lượng sản phẩm',
                'price.required'=>'Không được để trống giá sản phẩm',
            ]
            );
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $prod->category_id = $request->category_id;
            $prod->name = $request->name;
            $prod->slug = str_slug($request->name).rand(100000, 999999);
            $prod->short_description = $request->short_description;
            $prod->amount= $request->amount;
            $prod->price = $request->price;
            $prod->status = $request->status;
            if($request->hasFile('image')){
                $file = $request->image;
                $new_image_name = time().$file->getClientOriginalName();
                $prod->image = $new_image_name;
                $file->move('image/product',$new_image_name);
            }
            $prod->save();
            return redirect()->route('ad_prod.index')->with(['mess'=>'Cập nhật thành công']);
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prod = Product::findOrFail($id);
        $prod->delete();
        return back();
    }
}
