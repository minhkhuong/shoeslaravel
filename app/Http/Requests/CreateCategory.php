<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category"=>"required",
            "category.name"=>"required|string|max:255",
            "category.status"=>"required|string",
            "category.description"=>"required|string|max:800",
            "category.image"=>"nullable",
        ];
    }
}
