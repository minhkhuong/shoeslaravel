<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product'=>'required',
            'product.name'=>'string|max:255',
            'product.short_description'=>'string|max:800',
            'product.amount'=>'int',
            'product.category_id'=>'numeric',
        ];
    }
}
