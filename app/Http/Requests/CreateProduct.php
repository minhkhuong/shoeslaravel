<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "product"=>"required",
            "product.name"=>"required|string|max:255",
            "product.short_description"=>"required|string|max:800",
            "product.status"=>"required|string",
            "product.amount"=>"required|int",
            "product.price"=>"required|int",
            "product.category_id"=>"required|numeric",
            "product.image"=>"nullable",

        ];
    }
}
