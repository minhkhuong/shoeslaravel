<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Models\Category;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.guest.master',function($view){
            $view->with(['dropcate'=>Category::whereStatus(1)->get()]);
        });
      
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
