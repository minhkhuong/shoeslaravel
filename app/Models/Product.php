<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
        'name',
        'slug',
        'status',
        'short_description',
        'amount',
        'image',
        'price',
        'category_id'
    ];

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }
    public function orders(){
        return $this->belongsToMany('App\Models\Order','product_orders');
    }
    public function productOrders(){
        return $this->hasMany('App\Models\ProductOrder');
    }
    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }
    public function status(){
        if($this->status == 1){
            echo"Hoạt động";
        }else{
            echo"Không hoạt động";
        }
    }
    public function scopeFilterCategory($query ,$searchCate){
        if(!empty($searchCate)){
            $query->where('category_id',$searchCate);
        }
    }
    public function scopeFilterPrice($query, $searchPrice){
        if(!empty($searchPrice)){
            if($searchPrice == 1){
               return $query->whereBetween('price',[1000000,2000000]);
            }
            if($searchPrice == 2){
                return $query->whereBetween('price',[2000000,4000000]);
            }
            if($searchPrice == 3){
                return $query->whereBetween('price',[4000000,10000000]);
            }
            if($searchPrice == 4){
                return $query->whereBetween('price',[10000000,20000000]);
            }
        }
    }
    public function scopeFilterName($query, $searchName)
    {
        if(!empty($searchName)){
            $query->where('name','LIKE',"%{$searchName}%")->whereStatus(1);
            
        }
    }
}
