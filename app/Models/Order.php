<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=[
        'order_number',
        'note',
        'total',
        'customer_address',
        'order_satus',
        'customer_name',
        'customer_phone',
        'customer_address',
        'order_status',
        'type_checkout',
        'user_id',
    ];
    public function products(){
        return $this->belongsToMany('App\Models\Product','product_orders')->withPivot('amount');
    }
    public function productOrders(){
        return $this->hasMany('App\Models\ProductOrder');
    }
    public function getOrderStatus(){
        if($this->order_status == 'M'){
            echo 'Đơn hàng mới';
        }elseif($this->order_satus == 'DG'){
            echo 'Đơn hàng đang giao';
        }elseif($this->order_satus == 'GR'){
            echo 'Đơn hàng giao rồi';
        }elseif($this->order_satus == 'H'){
            echo 'Đơn hàng đã hủy';
        }
    }
}
